; ----------------
; Generated makefile from http://drushmake.me
; Permanent URL: http://drushmake.me/file.php?token=a7baec8effba
;testing
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.

projects[commerce_kickstart][type] = "core"
projects[commerce_kickstart][version] = "1"


  
  
; Modules
; --------

projects[] = seo_checklist
projects[] = token
projects[] = pathauto
projects[] = page_title
projects[] = webform
projects[] = nodesinblock
projects[commerce_shipping][version] = "2"
projects[] = commerce_flat_rate
projects[wysiwyg][version] = "2"
projects[imce][version] = "1"
projects[imce_wysiwyg][version] = "1"
projects[stringoverrides][version] = "1"
projects[commerce_paypal][version] = "1"


; Themes
; --------

projects[adaptivetheme][version] = "2"
projects[at-commerce][version] = "2.0-rc3"

projects[myblugear][download][type] = "git"
projects[myblugear][download][url] = "https://bitbucket.org/leevh/myblugear-d7-theme.git"
projects[myblugear][type] = "theme"
projects[myblugear][download][branch] = "master"



  
  
; Libraries
; ---------


;NOTES

; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.4/ckeditor_3.6.4.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

; Please fill the following out. Type may be one of get, cvs, git, bzr or svn,
; and url is the url of the download.
libraries[ckfinder][download][type] = "get"
libraries[ckfinder][download][url] = "http://download.cksource.com/CKFinder/CKFinder%20for%20PHP/2.2.2.1/ckfinder_php_2.2.2.1.zip"
libraries[ckfinder][directory_name] = "ckfinder"
libraries[ckfinder][type] = "library"





; NOTES AND PATCHES ETC

; Had to patch Commerce Paypal module with http://drupal.org/node/1301570#comment-6956052 to allow address to be passed along to paypal.  



